db.users.insertMany(
	[
	{
		"firstName":"Diane",
		"lastName":"Murphy",
		"email":"dmurphy@gmail.com",
		"isAdmin":"no",
		"isActive":"yes"
	},
	{
		"firstName":"Mary",
		"lastName":"Patterson",
		"email":"mpatterson@gmail.com",
		"isAdmin":"no",
		"isActive":"yes"
	},
	{
		"firstName":"Jeff",
		"lastName":"Firrelli",
		"email":"jfirrelli@gmail.com",
		"isAdmin":"no",
		"isActive":"yes"
	},
	{
		"firstName":"Gerard",
		"lastName":"Bondur",
		"email":"gbondur@gmail.com",
		"isAdmin":"no",
		"isActive":"yes"
	},
	{
		"firstName":"Pamela",
		"lastName":"Castillo",
		"email":"pcastillo@gmail.com",
		"isAdmin":"yes",
		"isActive":"no"
	},
	{
		"firstName":"George",
		"lastName":"Vanauf",
		"email":"gvanauf@gmail.com",
		"isAdmin":"yes",
		"isActive":"yes"
	}

]);


db.courses.insertMany(
	[
	{
		"Name":"Professional Development",
		"Price":"10,000"
	},
	{
		"Name":"Business Processing",
		"Price":"13,000"
	}

]);


db.courses.updateMany(
	{ 
        "_id" : ObjectId("6125b352c405060923458737")
    },
    {$set: { "Enrollee": 
			[
				{
					"lastName" :ObjectId("6125b33dc405060923458731"),
				},
				{
					"lastName" :ObjectId("6125b33dc405060923458733"),
				}
			]
			});


db.courses.updateMany(
	{ 
        "_id" : ObjectId("6125b352c405060923458737")
    },
    {$set: { "Enrollee": 
    [
    {"lastName":ObjectId("6125b33dc405060923458731")},
    {"lastName":ObjectId("6125b33dc405060923458733")}
    ]

}});

db.courses.updateMany(
	{ 
        "_id" : ObjectId("6125b352c405060923458738")
    },
    {$set: { "Enrollee": 
    [
    {"lastName":ObjectId("6125b33dc405060923458734")},
    {"lastName":ObjectId("6125b33dc405060923458732")}
    ]

}});

db.users.find({
	"isAdmin":"no"
});